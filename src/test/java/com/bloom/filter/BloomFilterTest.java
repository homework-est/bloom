package com.bloom.filter;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BloomFilterTest {

  private double falsePositiveProbability = 0.001;
  private BloomFilterHandler bloomFilterHandler;

  @BeforeEach
  public void setup() {
    bloomFilterHandler = new BloomFilterHandler(falsePositiveProbability);
  }

  @Test
  void successfulWordSearchTest() {
    assertThat(bloomFilterHandler.isWordExist("coeducation")).isTrue();
  }

  @Test
  void unsuccessfulWordSearchTest() {
    assertThat(bloomFilterHandler.isWordExist("coeducation123")).isFalse();
  }
}
