package com.bloom.filter.dictionary;

import com.bloom.exception.ApplicationException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class SampleDictionary implements Dictionary {
  private static SampleDictionary sampleDictionary = null;
  private Set<String> words;

  private SampleDictionary() {
    this.words = readDictionarySource();
  }

  public static SampleDictionary getInstance() {
    if(sampleDictionary == null) {
      sampleDictionary = new SampleDictionary();
    }
    return sampleDictionary;
  }

  @Override
  public Set<String> getWords() {
    return this.words;
  }

  private Set<String> readDictionarySource() {
    final Set<String> words = new HashSet();
    final Path path = Paths.get("dictionary/sample-1.txt");
    try (Stream<String> stream = Files.lines(path, StandardCharsets.UTF_8)) {
      stream.forEach(words::add);
    } catch (IOException e) {
      throw new ApplicationException("Problems in reading dictionary source", e);
    }
    return words;
  }
}
