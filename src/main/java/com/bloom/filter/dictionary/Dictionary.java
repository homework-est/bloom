package com.bloom.filter.dictionary;

import java.util.Set;

public interface Dictionary {
  Set<String> getWords();
}
