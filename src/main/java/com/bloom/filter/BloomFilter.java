package com.bloom.filter;

import static java.lang.Math.floor;
import static java.lang.Math.log;

import com.bloom.filter.provider.HashFunctionProvider;
import com.bloom.filter.provider.HashProvider;
import java.util.ArrayList;
import java.util.List;

public class BloomFilter {
  private final int[] bitmap;
  private final int contentSize;
  private final int bitMapSize;
  private final List<HashProvider> hashFunctions = HashFunctionProvider.build();

  public BloomFilter(int contentSize, double falsePositiveProbability) {
    this.contentSize = contentSize;
    this.bitMapSize = getBitMapSize(contentSize, falsePositiveProbability);
    this.bitmap = new int[bitMapSize];
  }

  public void add(String value) {
    for (HashProvider hashProvider : hashFunctions) {
      int hashPosition = hashProvider.getHash(value, bitMapSize);
      bitmap[hashPosition] = 1;
    }
  }

  public boolean mightContain(String value) {
    final List<Integer> hashResult = new ArrayList<>();
    for (HashProvider hashProvider : hashFunctions) {
      int hashPosition = hashProvider.getHash(value, bitMapSize);
      hashResult.add(bitmap[hashPosition]);
    }
    return hashResult.stream().allMatch(integer -> integer == 1);
  }

  private int getBitMapSize(int contentSize, double falsePositiveProbability) {
    return (-1 * (contentSize * log2(falsePositiveProbability) / (log2(2) * log2(2))));
  }

  private int log2(double f) {
    return (int) floor(log(f) / log(2.0));
  }
}
