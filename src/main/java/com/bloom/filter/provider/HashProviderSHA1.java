package com.bloom.filter.provider;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.math.BigInteger;
import java.security.MessageDigest;

public class HashProviderSHA1 implements HashProvider{

  @Override
  public int getHash(String string, int wordsCount) {
    final MessageDigest md = getDigest("SHA-1");
    md.reset();
    md.update(string.getBytes(UTF_8));
    BigInteger bigInteger = new BigInteger(1, md.digest());
    return bigInteger.mod(BigInteger.valueOf(wordsCount)).intValue();
  }
}
