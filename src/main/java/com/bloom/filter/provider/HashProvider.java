package com.bloom.filter.provider;

import com.bloom.exception.ApplicationException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashProvider {

  default MessageDigest getDigest(String hashMethod) {
    try {
      return MessageDigest.getInstance(hashMethod);
    } catch (NoSuchAlgorithmException e) {
      throw new ApplicationException("No such algorithm present", e);
    }
  }

  int getHash(String string, int wordsCount);
}
