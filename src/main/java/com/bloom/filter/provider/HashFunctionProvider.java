package com.bloom.filter.provider;

import static java.util.Arrays.asList;

import java.util.List;

public class HashFunctionProvider {

  public static List<HashProvider> build() {
    return asList(new HashProviderMD5(), new HashProviderSHA1());
  }
}
