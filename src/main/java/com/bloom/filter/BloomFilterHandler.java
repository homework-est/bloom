package com.bloom.filter;

import com.bloom.filter.dictionary.SampleDictionary;

public class BloomFilterHandler {
  private BloomFilter bloomFilter;

  public BloomFilterHandler(double falsePositiveProbability) {
    buildBloomFilter(falsePositiveProbability);
  }

  private void buildBloomFilter(double falsePositiveProbability) {
    final SampleDictionary sampleDictionary = SampleDictionary.getInstance();
    bloomFilter = new BloomFilter(sampleDictionary.getWords().size(), falsePositiveProbability);
    sampleDictionary.getWords().stream().forEach(word -> bloomFilter.add(word));
  }

  public boolean isWordExist(String word) {
    return bloomFilter.mightContain(word);
  }
}
