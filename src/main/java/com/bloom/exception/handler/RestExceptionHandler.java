package com.bloom.exception.handler;

import com.bloom.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ApplicationException.class)
  protected ResponseEntity<Object> handleApplicationException(ApplicationException applicationException) {
    return new ResponseEntity<>(applicationException.getMessage(), HttpStatus.BAD_REQUEST);
  }
}
