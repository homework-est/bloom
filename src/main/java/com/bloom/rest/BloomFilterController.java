package com.bloom.rest;

import com.bloom.services.BloomFilterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/bloom")
@RequiredArgsConstructor
public class BloomFilterController {

  private final BloomFilterService bloomFilterService;

  @GetMapping("/search/{word}")
  public ResponseEntity search(@PathVariable String word) {
    boolean isPresent = bloomFilterService.isWordExist(word);
    return ResponseEntity.ok(isPresent);
  }
}
