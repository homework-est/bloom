package com.bloom;

import com.bloom.filter.BloomFilterHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BloomApplication {

	@Value("${bloomfilter.false-positive.probability}")
	private double falsePositiveProbability;

	public static void main(String[] args) {
		SpringApplication.run(BloomApplication.class, args);
	}

	@Bean
	BloomFilterHandler getBloomFilterHandler() {
		return new BloomFilterHandler(falsePositiveProbability);
	}
}
