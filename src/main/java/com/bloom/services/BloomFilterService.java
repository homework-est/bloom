package com.bloom.services;

import com.bloom.filter.BloomFilterHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BloomFilterService {

  private final BloomFilterHandler bloomFilterHandler;

  public boolean isWordExist(String word) {
    return bloomFilterHandler.isWordExist(word);
  }
}
