# Bloom filter


## About

This is bloom filter implementation. Two hashing is used MD5 and SHA-1. The false positive probability is declared in properties file. 

## Description
It is spring boot implementation. 

## Running the project
Please run following commands on project directory.
```
mvn package
java -jar bloom-0.0.1.jar
```

Once jar is running, Please make API call as follows to check if word exists in dictionary or not.

http://localhost:8080/rest/bloom/search/{word}

for example,
http://localhost:8080/rest/bloom/search/coeducation 
